.DEFAULT_GOAL := build

CP = cp -f
RM = rm -rf
MV = mv
ECHO = echo

q2pro:
	@$(MAKE) -C src/q2pro
	@$(CP) -a src/q2pro/q2pro dday-normandy
	@$(CP) -a src/q2pro/q2proded dday-normandy-ded

dday/config.cfg:
	@$(CP) -a dday/config.cfg.sample dday/config.cfg

dday: dday/config.cfg
	@$(MAKE) -C src
	@$(MV) src/game?*.real.* dday/

q2admin:
	@$(MAKE) -C src/q2admin
	@$(MV) src/q2admin/game?*.* dday/

build: q2pro dday q2admin

clean:
	@$(MAKE) clean -C src >/dev/null
	@$(MAKE) clean -C src/q2admin >/dev/null
	@$(MAKE) clean -C src/q2pro >/dev/null
	@$(RM) dday-normandy >/dev/null
	@$(RM) dday-normandy-ded >/dev/null
	@$(RM) dday/config.cfg >/dev/null
	@$(RM) dday/game?*.* >/dev/null
	@$(RM) dday/game?*.real.* >/dev/null
	@$(ECHO) "Successfully removed"
.PHONY: q2pro dday q2admin build clean
